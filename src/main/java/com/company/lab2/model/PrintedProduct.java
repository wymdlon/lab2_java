package com.company.lab2.model;

import com.company.lab2.annotation.Column;
import com.company.lab2.annotation.Table;

import java.util.Objects;

public abstract class PrintedProduct {

    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "number_of_page")
    private Integer numberOfPages;
    @Column(name = "year_of_issue")
    private Integer yearOfIssue;

    public PrintedProduct(Long id, String name, Integer numberOfPages, Integer yearOfIssue) {
        this.id = id;
        this.name = name;
        this.numberOfPages = numberOfPages;
        this.yearOfIssue = yearOfIssue;
    }

    public PrintedProduct() { }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrintedProduct that = (PrintedProduct) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(numberOfPages, that.numberOfPages) &&
                Objects.equals(yearOfIssue, that.yearOfIssue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, numberOfPages, yearOfIssue);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(Integer numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public Integer getYearOfIssue() {
        return yearOfIssue;
    }

    public void setYearOfIssue(Integer yearOfIssue) {
        this.yearOfIssue = yearOfIssue;
    }
}
