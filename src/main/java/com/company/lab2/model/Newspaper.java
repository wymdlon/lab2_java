package com.company.lab2.model;

import com.company.lab2.annotation.Column;
import com.company.lab2.annotation.Table;

import java.util.Objects;

@Table(name = "printed_product", type = 2)
public class Newspaper extends PrintedProduct {
    @Column(name = "publisher")
    protected String publisher;

    public Newspaper(Long id, String name, Integer numberOfPages, Integer yearOfIssue, String publisher) {
        super(id, name, numberOfPages, yearOfIssue);
        this.publisher = publisher;
    }

    public Newspaper() { }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Newspaper newspaper = (Newspaper) o;
        return Objects.equals(publisher, newspaper.publisher);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), publisher);
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
}
