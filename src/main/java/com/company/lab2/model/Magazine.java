package com.company.lab2.model;

import com.company.lab2.annotation.Column;
import com.company.lab2.annotation.Table;

import java.util.Objects;

@Table(name = "printed_product", type = 3)
public class Magazine extends PrintedProduct {
    @Column(name = "price")
    protected Double price;

    public Magazine(Long id, String name, Integer numberOfPages, Integer yearOfIssue, Double price) {
        super(id, name, numberOfPages, yearOfIssue);
        this.price = price;
    }

    public Magazine() { }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Magazine magazine = (Magazine) o;
        return Objects.equals(price, magazine.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), price);
    }
}
