package com.company.lab2.model;

import com.company.lab2.annotation.Column;
import com.company.lab2.annotation.Table;

import java.util.Date;
import java.util.Objects;

@Table(name = "printed_product", type = 1)
public class Book extends PrintedProduct {
    @Column(name = "author")
    protected String author;
    @Column(name = "price")
    protected Double price;

    public Book(Long id, String name, Integer numberOfPages, Integer yearOfIssue, String author, Double price) {
        super(id, name, numberOfPages, yearOfIssue);
        this.author = author;
        this.price = price;
    }

    public Book() { }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Book book = (Book) o;
        return Objects.equals(author, book.author) &&
                Objects.equals(price, book.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), author, price);
    }
}
