package com.company.lab2.dao;

import com.company.lab2.annotation.Column;
import com.company.lab2.annotation.Table;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DaoImpl<T> implements IDaoImpl<T> {
    private String connectionString;
    private Class<T> tClass;

    public DaoImpl(String connectionString, Class<T> clazz) {
        this.connectionString = connectionString;
        this.tClass = clazz;
    }

    private T mapTableToEntity(ResultSet resultSet) {
        T entity = null;
        try {
            entity = tClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        Class currentClass = tClass;
        List<Field> fieldsList = new ArrayList<>();
        while (currentClass.getSuperclass() != null) {
            Field[] fields = currentClass.getDeclaredFields();
            fieldsList.addAll(Arrays.asList(fields));
            currentClass = currentClass.getSuperclass();
        }

        for (Field field : fieldsList) {
            String attribute = field.getAnnotation(Column.class).name();
            String fieldName = field.getName();
            try {
                Object value = resultSet.getObject(attribute);
                if (fieldName == "id" && value instanceof Integer)
                    value = ((Integer)value).longValue();
                else value = field.getType().cast(value);

                field.setAccessible(true);
                field.set(entity, value);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return entity;
    }

    private String getTableName() {
        return tClass.getAnnotation(Table.class).name();
    }
    private int getTableType() {
        return tClass.getAnnotation(Table.class).type();
    }

    //private

    @Override
    public T getEntity(Long id) {
        T entity = null;
        try  {
            Connection connection = DriverManager.getConnection(connectionString, "postgres", "15funuru");

            // When this class first attempts to establish a connection, it automatically loads any JDBC 4.0 drivers found within
            // the class path. Note that your application must manually load any JDBC drivers prior to version 4.0.
            Class.forName("org.postgresql.Driver");

            String tableName = getTableName();
            int type = getTableType();

            String sqlStr = "SELECT * FROM \"" + tableName + "\" WHERE id = ?";
            sqlStr += (type != 0) ? " AND type = ?" : "";

            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
//            preparedStatement.setString(1, tableName);
            preparedStatement.setLong(1, id);
            if (type != 0)
                preparedStatement.setInt(2, type);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                entity = mapTableToEntity(resultSet);
            }

        } /*catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC driver not found.");
            e.printStackTrace();
        }*/ catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public List<T> getEntities() {
        List<T> entities = new ArrayList<>();
        try  {
            Connection connection = DriverManager.getConnection(connectionString, "postgres", "15funuru");

            // When this class first attempts to establish a connection, it automatically loads any JDBC 4.0 drivers found within
            // the class path. Note that your application must manually load any JDBC drivers prior to version 4.0.
            Class.forName("org.postgresql.Driver");

            String tableName = getTableName();
            int type = getTableType();

            String sqlStr = "SELECT * FROM \"" + tableName + "\"";
            sqlStr += (type != 0) ? " WHERE type = ?" : "";

            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);

            if (type != 0)
                preparedStatement.setInt(1, type);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                T entity = mapTableToEntity(resultSet);
                entities.add(entity);
            }

        }
        catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return entities;
    }
}
