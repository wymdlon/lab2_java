package com.company.lab2.dao;

import com.company.lab2.model.Book;
import com.company.lab2.model.Magazine;
import com.company.lab2.model.Newspaper;
import com.company.lab2.model.User;

import java.util.Collection;
import java.util.List;

public class DAO implements IDAO {
    private IDaoImpl<User> userDaoImpl;
    private IDaoImpl<Newspaper> newspaperDaoImpl;
    private IDaoImpl<Book> bookDaoImpl;
    private IDaoImpl<Magazine> magazineDaoImpl;

    public DAO(String connectionString) {
        this.userDaoImpl = new DaoImpl<User>(connectionString, User.class);
        this.newspaperDaoImpl = new DaoImpl<Newspaper>(connectionString, Newspaper.class);
        this.bookDaoImpl = new DaoImpl<Book>(connectionString, Book.class);
        this.magazineDaoImpl = new DaoImpl<Magazine>(connectionString, Magazine.class);
    }

    @Override
    public User getUserById(Long id) {
        return userDaoImpl.getEntity(id);
    }

    @Override
    public List<User> getAllUsers() {
        return userDaoImpl.getEntities();
    }

    @Override
    public Book getBookById(Long id) {
        return bookDaoImpl.getEntity(id);
    }

    @Override
    public List<Book> getAllBooks() {
        return bookDaoImpl.getEntities();
    }

    @Override
    public Newspaper getNewspaperById(Long id) {

        return newspaperDaoImpl.getEntity(id);
    }

    @Override
    public List<Newspaper> getAllNewspapers() {
        return newspaperDaoImpl.getEntities();
    }

    @Override
    public Magazine getMagazineById(Long id) {
        return magazineDaoImpl.getEntity(id);
    }

    @Override
    public List<Magazine> getAllMagazines() {
        return magazineDaoImpl.getEntities();
    }
}
