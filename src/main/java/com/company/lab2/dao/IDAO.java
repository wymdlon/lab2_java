package com.company.lab2.dao;

import com.company.lab2.model.Book;
import com.company.lab2.model.Magazine;
import com.company.lab2.model.Newspaper;
import com.company.lab2.model.User;

import java.util.List;

public interface IDAO {
    User getUserById(Long id);
    List<User> getAllUsers();

    Book getBookById(Long id);
    List<Book> getAllBooks();

    Newspaper getNewspaperById(Long id);
    List<Newspaper> getAllNewspapers();

    Magazine getMagazineById(Long id);
    List<Magazine> getAllMagazines();
}
