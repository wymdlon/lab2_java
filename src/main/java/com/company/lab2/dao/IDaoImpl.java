package com.company.lab2.dao;

import java.util.List;

public interface IDaoImpl<T> {
    T getEntity(Long id);
    List<T> getEntities();
}
