import com.company.lab2.dao.*;
import com.company.lab2.model.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestDao {
    private  IDAO dao;

    public TestDao() {
        String connectionString = "jdbc:postgresql://localhost:5432/lab2_java";
        this.dao = new DAO(connectionString);
    }

    @Test
    public void getUserById_shouldReturnUserById() {
        //expected
        User exp = new User( 1L, "Vlad", "Demchenko", 20);
        //actual
        User act = dao.getUserById(1L);

        Assert.assertEquals(exp, act);
    }

    @Test
    public void getAllUsers_shouldReturnAllUsers() {
        //expected
        User user1 = new User( 1L, "Vlad", "Demchenko", 20);
        User user2 = new User( 2L, "Ivan", "Ivanov", 15);
        User user3 = new User( 3L, "Valeriy", "Pagyloy", 54);

        List<User> expectedUsers = new ArrayList<>();
        expectedUsers.add(user1);
        expectedUsers.add(user2);
        expectedUsers.add(user3);

        //actual
        List<User> actualUsers = dao.getAllUsers();

        Assert.assertEquals(expectedUsers.size(), actualUsers.size());
        for (int i = 0; i < expectedUsers.size(); i++) {
            Assert.assertEquals(expectedUsers.get(i), actualUsers.get(i));
        }
    }

    @Test
    public void getBookById_shouldReturnBookById() {
        //expected
        Book exp = new Book(
                1L,
                "The witcher 1",
                100,
                1992,
                "Andrzej Sapkowski",
                200.0);
        //actual
        Book act = dao.getBookById(1L);

        Assert.assertEquals(exp, act);
    }

    @Test
    public void getAllBooks_shouldReturnAllBooks() {
        //expected
        Book book1 = new Book(
                1L,
                "The witcher 1",
                100,
                1992,
                "Andrzej Sapkowski",
                200.0);
        Book book2 = new Book(
                2L,
                "The witcher 2",
                200,
                1995,
                "Andrzej Sapkowski",
                600.0);
        Book book3 = new Book(
                3L,
                "The witcher 3",
                300,
                1999,
                "Andrzej Sapkowski",
                400.0);

        List<Book> expectedBooks = new ArrayList<>();
        expectedBooks.add(book1);
        expectedBooks.add(book2);
        expectedBooks.add(book3);

        //actual
        List<Book> actualBooks = dao.getAllBooks();

        Assert.assertEquals(expectedBooks.size(), actualBooks.size());
        for (int i = 0; i < expectedBooks.size(); i++) {
            Assert.assertEquals(expectedBooks.get(i), actualBooks.get(i));
        }
    }

    @Test
    public void getNewspaperById_shouldReturnNewspaperById() {
        //expected
        long id = 4L;
        Newspaper exp = new Newspaper(
                id,
                "New York Times 1",
                15,
                2019,
                "New York Times Inc.");
        //actual
        Newspaper act = dao.getNewspaperById(id);

        Assert.assertEquals(exp, act);
    }

    @Test
    public void getAllNewspapers_shouldReturnAllNewspapers() {
        //expected
        Newspaper newspaper1 = new Newspaper(
                4L,
                "New York Times 1",
                15,
                2019,
                "New York Times Inc.");
        Newspaper newspaper2 = new Newspaper(
                5L,
                "New York Times 2",
                23,
                2019,
                "New York Times Inc.");
        Newspaper newspaper3 = new Newspaper(
                6L,
                "New York Times 3",
                32,
                2019,
                "New York Times Inc.");

        List<Newspaper> expectedNewspapers = new ArrayList<>();
        expectedNewspapers.add(newspaper1);
        expectedNewspapers.add(newspaper2);
        expectedNewspapers.add(newspaper3);

        //actual
        List<Newspaper> actualNewspapers = dao.getAllNewspapers();

        Assert.assertEquals(expectedNewspapers.size(), actualNewspapers.size());
        for (int i = 0; i < expectedNewspapers.size(); i++) {
            Assert.assertEquals(expectedNewspapers.get(i), actualNewspapers.get(i));
        }
    }

    @Test
    public void getMagazineById_shouldReturnMagazineById() {
        //expected
        Magazine exp = new Magazine(
                7L,
                "IGM",
                25,
                2019,
                50.0);
        //actual
        Magazine act = dao.getMagazineById(7L);

        Assert.assertEquals(exp, act);
    }

    @Test
    public void getAllMagazines_shouldReturnAllMagazines() {
        //expected
        Magazine magazine1 = new Magazine(
                7L,
                "IGM",
                25,
                2019,
                50.0);
        Magazine magazine2 = new Magazine(
                8L,
                "Vogue",
                20,
                2019,
                100.0);
        Magazine magazine3 = new Magazine(
                9L,
                "Forbes",
                30,
                2019,
                200.0);

        List<Magazine> expectedMagazines = new ArrayList<>();
        expectedMagazines.add(magazine1);
        expectedMagazines.add(magazine2);
        expectedMagazines.add(magazine3);


        //actual
        List<Magazine> actualMagazines = dao.getAllMagazines();

        Assert.assertEquals(expectedMagazines.size(), actualMagazines.size());
        for (int i = 0; i < expectedMagazines.size(); i++) {
            Assert.assertEquals(expectedMagazines.get(i), actualMagazines.get(i));
        }
    }
}
